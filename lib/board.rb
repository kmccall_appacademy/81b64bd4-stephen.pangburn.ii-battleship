class Board
  attr_reader :grid

  def initialize(grid = nil)
    if grid.nil?
      @grid = Board::default_grid
      populate_grid(4)
    else
      @grid = grid
    end
  end

  def self.default_grid
    [[nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
     [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
     [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
     [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
     [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
     [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
     [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
     [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
     [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
     [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil]]
  end

  def display
    @grid.each do |row|
      row.each do |elem|
        if elem.nil? # || elem == :s
          print " ."
        else
          print " #{elem.to_s}"
        end
      end
      puts
    end
  end

  def place_random_ship
    raise "Board is full." if full?
    loop do
      x = rand(0...@grid[0].length)
      y = rand(0...@grid.length)

      if empty?([x, y])
        @grid[x][y] = :s
        break
      end
    end
  end

  def populate_grid(num_ships)
    num_ships.times do
      place_random_ship
    end
  end

  def count
    ships = 0

    @grid.each do |row|
      row.each do |elem|
        ships += 1 if elem == :s
      end
    end
    ships
  end

  def in_range?(pos)
    x, y = pos
    (0...@grid[0].length).include?(x) && (0...@grid.length).include?(y)
  end

  def empty?(pos = nil)
    if pos.nil?
      @grid.all? do |row|
        row.all? do |elem|
          elem == nil
        end
      end
    else
      self[pos].nil?
    end
  end

  def full?
    @grid.all? do |row|
      row.all? do |elem|
        elem != nil
      end
    end
  end

  def won?
    @grid.none? do |row|
      row.any? do |elem|
        elem == :s
      end
    end
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end
end
