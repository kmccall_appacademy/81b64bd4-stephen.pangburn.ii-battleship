class HumanPlayer

  def get_play
    input = gets.chomp
    move = input.match(/^\(?(\d+),?\s*(\d+)\)?/)

    [move[1].to_i, move[2].to_i]
  end
end
