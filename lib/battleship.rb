require_relative "player"
require_relative "board"

class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new, board = Board.new)
    @player = player
    @board = board
  end

  def play
    puts "\nBattleship v.0.0.1\n-------------------\n"
    display_status
    loop do
      play_turn
      display_status

      if game_over?
        puts "Congratulations! You won!\n\n"
        break
      end
    end
  end

  def attack(pos)
    @board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won? || @board.full?
  end

  def play_turn
    print "Please enter the coordinates of your move: "
    loop do
      move = player.get_play
      if @board.in_range?(move)
        attack(move)
        break
      else
        print "Invalid move. Please try again: "
      end
    end
  end

  def display_status
    @board.display
    puts "Ships remaining: #{count}\n\n"
  end
end

if __FILE__ == $PROGRAM_NAME
  BattleshipGame.new.play
end
